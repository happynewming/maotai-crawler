require('dotenv').config()
const axios = require('axios')
const cheerio = require('cheerio');
const xlsx =  require('node-xlsx');
const fs = require('fs');

async function fetchPage () {
  try {
    res = await axios.get(process.env.CRAWLER_URL)
    return res.data
  } catch (error) {
    console.log(error)
  }
  return undefined
}

async function main () {
  const titles = []
  const vals = []

  const htmlStr = await fetchPage()
  if (htmlStr) {
    const $ = cheerio.load(htmlStr)
    const table = $('table.table_bg001.border_box.limit_sale.scr_table')

    const trs = table.children().children()
    trs.first().children().each(function (i, e) {
      titles.push($(this).text());
    });
    trs.eq(11).children().each(function (i, e) {
      vals.push($(this).text())
    })
  }

  const data = [titles, vals]; // 把数组写入excel
  const buffer = xlsx.build([
    {
      name: process.env.FILE_NAME,
      data,
    },
  ]);

  fs.writeFileSync(`${process.env.FILE_NAME}.xlsx`, buffer)
}

main()